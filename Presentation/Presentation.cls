% ----------------------------- CLASS INFORMATION ------------------------------
% Version of LaTeX to use.
\NeedsTeXFormat{LaTeX2e}

 % Name of the class.
\newcommand{\@classname}{presentation}
\ProvidesClass{\@classname}
% ------------------------------------------------------------------------------


% ------------------------------- GENERAL STYLE --------------------------------
% Document class to build this class upon.
\LoadClass{beamer}

% Set beamer theme.
\usetheme{default}
\usecolortheme{whale}

% Main font.
\RequirePackage{lmodern}

% Monospace font (teletypewriter font).
\RequirePackage{courier}

% Caption style.
\RequirePackage{caption} % Enable captions.
\renewcommand{\captionlabelfont}{\bfseries}
\renewcommand{\captionfont}{\small}

% Remove navigation bar.
\beamertemplatenavigationsymbolsempty 

% Add frame number. 
\setbeamerfont{footline}{size=\small}
\setbeamertemplate{footline}{
    \begin{flushright}
        \textcolor{gray}{ % Color of the frame number.
            \vspace{-5pt}
            \scriptsize{\insertframenumber/\inserttotalframenumber}
            \hspace{3pt}
        }
    \end{flushright}
}

% Unordered list style.
\setbeamertemplate{itemize items}[circle]

% Table of contents item style.
\setbeamertemplate{section in toc}[square]
% ------------------------------------------------------------------------------


% --------------------------- LANGUAGE AND ENCODING ----------------------------
% Encoding of the document source.
\RequirePackage[utf8]{inputenc}

% Document language.
\RequirePackage[croatian]{babel}

% Better support for quotation signs (context and language aware).
\RequirePackage{csquotes}

% Minimum number of letters before and after in order for hyphenation to occur
% for Croatian language. Fist digit is number of letters before the hyphenation
% and second is after hyphenation.
\providehyphenmins{croatian}{77}

% Command for English term in Croatian text.
\newcommand{\engl}[1]{(engl.~\emph{#1})}
% ------------------------------------------------------------------------------


% ----------------------- MANDATORY DOCUMENT INFORMATION -----------------------
% Command for defining the thesis number. Make it mandatory.
\newcommand{\thesisnumber}[1]{\renewcommand{\@thesisnumber}{#1}}
\newcommand{\@thesisnumber}{ % Error if no thesis number has been defined.
    \ClassError{\@classname}{No thesis number defined}
        {Define thesis number using the \noexpand\thesisnumber command.}}

% Command for defining the mentor. Make the information mandatory.
\newcommand{\mentor}[1]{\renewcommand{\@mentor}{#1}}
\newcommand{\@mentor}{ % Error if no mentor has been defined.
    \ClassError{\@classname}{No mentor number defined}
        {Define mentor using the \noexpand\mentor command.}}

% Make document author mandatory.
\renewcommand{\@author}{ % Error if no document author has been defined.
    \ClassError{\@classname}{No document author defined}
        {Define author using \noexpand\author command.}}

% Make document title mandatory.
\renewcommand{\@title}{ % Error if no document title has been defined.
    \ClassError{\@classname}{No document title defined}
        {Define title using \noexpand\title command.}}

% Make date mandatory.
\date{ % Error if no date has been defined.
    \ClassError{\@classname}{No date defined}
        {Define date using \noexpand\date command.}}
% ------------------------------------------------------------------------------


% --------------------------------- TITLE PAGE ---------------------------------
% Define title page.
\renewcommand{\maketitle}{
    {   % Scope setting of beamer template.
        \setbeamertemplate{footline}{} % Do not show frame number.
        \begin{frame}
            \titlepage
            \begin{center}
                \footnotesize{Mentor: \@mentor}
                \vspace{5pt}

                \footnotesize{Diplomski rad br. \@thesisnumber}
                \vspace{10pt}
        
                \begin{minipage}{0.55\textwidth}
                \begin{center}
                    \includegraphics[height=30pt]{{"FER Logo"}.png}\\[1cm]
                \end{center}
                \end{minipage}
                ~
                \begin{minipage}{0.4\textwidth}
                    \begin{center}
                        \includegraphics[height=40pt]{{"UNIZG Logo"}.png}\\[1cm]
                    \end{center}
                \end{minipage}
            \end{center}
        \end{frame}
    }
    \addtocounter{framenumber}{-1} % Do not count title frame.
}
% ------------------------------------------------------------------------------


% ----------------------------- PSEUDOCODE LISTING -----------------------------
% Enable pseudocode listing.
\RequirePackage[ruled]{algorithm2e}

% Translate pseudocode listing caption to Croatian. 
\renewcommand{\algorithmcfname}{Pseudokod}

% Environment for listing pseudocode in Croatian.
\newenvironment{pseudocode}
{
    \begin{algorithm}[H]
    \SetKwFor{ForEach}{za svako}{}{}
    \SetKwIF{If}{ElseIf}{Else}{ako}{}{inače ako}{inače}{}
}
{
    \end{algorithm}
}
% ------------------------------------------------------------------------------


% ------------------------------ COLORED EQUATIONS -----------------------------
% Enable colored textbox environment.
\RequirePackage{tcolorbox}

% Color the equations.
\newcommand{\coloredeqation}[1]{
    \begin{tcolorbox}[colback=white,colframe=black,
                      boxrule=0.65pt,arc=0pt,
                      boxsep=2pt,left=4pt,right=4pt,top=3pt,bottom=3pt]
        $#1$
    \end{tcolorbox}
}
% ------------------------------------------------------------------------------


% ------------------------------- HELPER COMMANDS ------------------------------
% Graphic centered on the slide.
\newcommand{\centergraphic}[2]{
    \begin{center}
            \includegraphics[#1]{#2}
    \end{center}
}

% Enable additional text symbols. Needed for center tilde. 
\RequirePackage{textcomp}
\newcommand{\textapprox}{\raisebox{0.5ex}{\texttildelow}}
% ------------------------------------------------------------------------------
