------------------------------- TITLE FRAME [0] --------------------------------
Predstavljanje sebe i rada.
--------------------------------------------------------------------------------


-------------------------------- MOTIVATION [1] --------------------------------
Moj diplomski rad vezan je uz projekt na kojem radim i koji se bavi poboljšanjem
sigurnosti aplikacija u oblaku, na način da se koristi više oblaka (npr. amazon,
dropbox i google). Zašto više oblaka? Zato što ne želimo vjerovati jednom
oblaku, a enkripcija ispada iz igre jer sprječava obradu u samom oblaku.

Kako bi smanjili razinu povjerenja koju treba imati u oblak, ideja je da se
podaci fragmentiraju i rasporede na više oblaka. Na taj način, svaki oblak ima
samo dio informacije. Tu dolazi glavna pretpostavka da je zbroj informacija
dijelova podataka manji od informacije cijelog skupa podatka. Na ovom primjeru,
ako bi netko imao informaciju o imenu i prezimenu i o isplati, onda bi mogao
izvesti i novu informaciju o tome tko ima koliko novaca. Kada bi netko imao samo
ime i prezime, znao bi kome su novci isplaćeni, ali ne bi znao iznos. Isto
tako, kada bi imao samo iznose, nebi znao kome pripada koji iznos.

Kako je zbroj informacija fragmenata manji od informacije kada su svi podaci
zajedno, onda je i zbroj povjerenja u pojedine cloudove također manji od
povjerenja kojeg bi trebali imati u jedan cloud kada bi sve podatke stavili na
njega. To je glavna motivacija za fragmentiranje podataka. I sada kada imamo
fragmente, treba ih pametno rasprediti na cloudove.
--------------------------------------------------------------------------------


---------------------------- APPLICATION MODEL [2] -----------------------------
Prvo treba definirati strukturu opisa aplikacije, odnosno ulaz u optimizaciju.
Taj input se sastoji od četiri dijela: popisa fragmenata, popisa komponenti,
opisa koje komponente koriste koje fragmente i opisa kako komponente međusobno
komuniciraju. Koristeći ove četiri informacije možemo jednoznačno nacrtati
ovakvu sliku.

Aplikacija sa slike ima tri fragmenta podataka označena sivim kvadratima, tri
komponente označene krugovima i dva međurezultata označena pravokutnicima na
strelicama.

Fragmenti podataka i međurezultati predstavljaju resurse koje se nastoji što
bolje zaštititi. Međurezultate je isto potrebno tretirati kao fragmente jer ne
možemo biti sigurni koliko je neka komponenta promjenila podatak prilikom
korištenja. Primjerice, međurezultat R02 može biti isti podatak kao i F0 jer C0
samo prosljeđuje ulaz na izlaz. Stoga, međurezultate valja tretirati isto kao i
fragmente.
--------------------------------------------------------------------------------


--------------------------- SECURITY CONSTRAINTS [3] ---------------------------
Najbitniji dio cijelog modela su sigurnosne zavisnosti, odnosno, sigurnosna
ograničenja. To je način da izrazimo da skup resursa nosi više informacija kada
je cijeli skup zajedno nego kada se sumiraju informacije pojedinih resursa u tom
skupu. Stoga, takve resurse ne želimo držati na istom mjestu kako bi smanjili
rizik.

Kad kažem mjesto, mislim na bilo što što omogućava pohranu fragmenata podataka i
izvršavanje aplikacijskih komponenti. To može predstavljati cjelokupnog
pružatelja cloud usluge, pojedine njegove poslužitelje, računala koja posjeduje
sam korisnik i tako dalje. Model radi neovisno o tome što je mjesto u stvarnom
svijetu. Zahtjeva se jedino da svako mjesto ima dodijeljenu razinu povjerenja.
Veće povjerenje znači da je manja vjerojatnost da na tom mjestu nešto pođe po
zlu.

Primjerice, ograničenje F0 i F1 znači da je bolje te fragmente držati na
različitim mjestima nego na jednom.
--------------------------------------------------------------------------------


----------------------------- RISK ASSESSMENT [4] ------------------------------
Metoda koju koristim za ocjenu koliko je neki razmještaj dobar je procjena
rizika. Da bi proveli procjenu rizika, treba odrediti resurse, odrediti važnost
svakog resursa i odrediti vjerojatnost ugrožavanja nekog resursa. Zatim rizik
računamo kao važnost puta vjerojatnost ugrožavanja.

Identificirali resurse jesmo: to su fragmenti podataka i međurezultati. Svakome
od njih treba dodijeliti važnost i vjerojatnost ugrožavanja. Važnost resursa
određuje korisnik jer on poznaje semantiku podataka. Vjerojatnost ugrožavanja
je obrnuto proporcionalna povjerenju u mjesto na kojem je resurs dostupan - to
povjerenje također određuje korisnik. Ako je resurs na sigurnijem mjestu,
vjerojatnost ugrožavanja je manja, drugim riječima, rizik je manji.

Prvi pseudokod prikazuje način računanja rizika resursa zasebno, nešto što ja
nazivam Single Resource rizik. Taj rizik pokazuje koliko je svaki resurs
pojedinačno ugrožen, odnosno ne uračunava odnos s drugim resursima. Ukupan
Single Resoursce rizik dobiva se tako da se iterira po svim mjestima, vidi se
koji resursi su tamo te se sumiraju omjeri važnosti i povjerenja. Ponavljam,
tako izračunati rizik prikazuje kolika je sigurnost fragmenata podataka i
međurezultata pojedinačno - ne gleda se njihov međusoban odnos, to jest, uopće
se ne gledaju sigurnosne zavisnosti.

Drugi način računanja rizika uzima u obzir sigurnosne ovisnosti. Ovdje se
kažnjava kršenje sigurnosnih zavisnosti fiksnom kaznom. U slučaju da neko
ograničenje nije moguće zadovoljiti, želimo da to ograničenje bude prekršeno na
što sigurnijem mjestu, pa je rizik ponovno obrnuto proporcionalan povjerenju u
mjesto kršenja zavisnosti.

Pomoću ova dva načina imamo dvije komponente sigurnosti o kojima smo na početku
projekta dosta raspravljali:
    1. Koliko su sigurni resursi zasebno?
    2. Koliko je siguran međusoban odnos resursa?
--------------------------------------------------------------------------------


---------------------------- RISK OPTIMIZATION [5] -----------------------------
Sad kad znamo izračunati rizik, možemo ga optimizirati. No, još ostaje problem
kako ta dva rizika iskombinirati? Pa, tu imamo dvije opcije:
    1. Linearna kombinacija ta dva kriterija i primjena jednokriterijskih
       algoritama
    2. Višekriterijska optimizacija

Linearna kombinacija ima veliki problem: korisnik treba unaprijed odrediti
koliko mu je koji kriterij važan tako da odredi koeficijente 'a' i 'b'. To je
veliki problem jer se ti kriteriji mogu razlikovati u redovima veličine, pa
strah od toga: "jesam li odabrao dobre koeficijente" je cijelo vrijeme u
primisli što nije baš super, osobito kada se bavimo sigurnošću.

Višekriterijska optimizacija rješava taj strah tako što dobrotu rješenja više ne
predstavlja jednim brojem već n-torkom. Posljedica toga su kompliciraniji
algoritmi jer usporedba rješenja nije trivijalna. Tipičan rad algoritma u
prostoru funkcija cilja prikazanoj na slici izgleda tako da se prvo rješenja
nasumično rasprše po prostoru i zatim algoritam nastoji gurati rješenja sve više
i više prema donjem lijevom kutu jer se oba kriterija nastoje minimizira. Na
kraju, korisnik bira jedno od ponuđenih rješenja i tek sad mora donijeti odluku
o važnosti kriterija. To mu je sada puno lakše jer vidi kako se kriteriji
međusobno ponašaju.

Primjerice, ako pogledamo ovu sliku i zamislimo da korisnik želi rješenja jako
dobra po onom prvom kriteriju koji gleda sigurnost resursa zasebno. Ima izbor od
ova tri rješenja s lijeve strane jer ta imaju mali single resource risk. No,
generalno gledajući, nema mu baš smisla uzeti ovo gore rješenje jer je ono puno
lošije glede sigurnosnih ograničenja, a samo malo bolje glede ovog prvog
kriterija.
--------------------------------------------------------------------------------


------------------------------ IMPLEMENTATION [6] ------------------------------
Implementacijski dio diplomskog rada odrađen je u koristeći C++. Napravljen je
omotač oko biblioteka za optimizaciju koji uzima opis aplikacije i konfiguraciju
algoritma te potom pokreće optimizacijski algoritam. Na kraju se korisniku
ponudi rješenje ili više njih u slučaju višekriterijske optimizacije.

Dana su dva jednokriterijska i dva višekriterijska algoritma. Jednokriterijski
su genetski algoritmi koji se međusobno razlikuju po operatoru selekcije.

Algoritam NSGA-III je verzija genetskog algoritma prilagođena za više kriterija
i pokazao se kao jedan od najboljih za višekriterijske probleme. Drugi
višekriterijski algoritam je Predator-Prey Evolution Strategy kojeg sam
implementirao na diplomskom projektu u zimskom semestru. Implementacijski je
prilično drugačiji od ostalih višekriterijskih algoritama, pa je zbog toga bio
zanimljiv.

Potrudio sam se napisati čitki i prenosivi kod visoke razine apstrakcije kako bi
daljni rad na projektu bio što lakši. Činilo mi se da ima smisla uložiti nešto
vremena u provjeru ispravnosti ulaza, unit-testing, komentiranje i slične
nefunckcionalnosti jer postoji dobra šansa da ću koristiti barem dijelove tog
koda i nakon diplomskog rada.
--------------------------------------------------------------------------------


------------------------------- EXPERIMENTS [7] --------------------------------
Nakon što sam na maloj aplikaciji utvrdio da stvar radi, izmislio sam veću
aplikaciju od deset fragmenata i deset komponenti. Takva aplikacija se treba
smjestiti na sedam mjesta. Takve postavke daju prostor pretraživanja veličine
sedam na dvadesetu.
--------------------------------------------------------------------------------


--------------------------------- RESULTS [8] ----------------------------------
Ova izmišljena aplikacija ne predstavlja problem za dobre algoritme - turnirski
genetski algoritam pronalazi najbolje rješenje u četrdesetak generacija što je
otprilike 0.4 sekunde na običnom desktopu. S proporcionalnom selekcijom imam
problema. Prebrzo se zabija u lokalni optimum i u niti pola slučajeva ne uspjeva
doći do optimalnog rješenja. Isprobao sam veće mutacije i veće populacije, bez
puno uspjeha. Nisam si uspio objasniti zašto je to tako, ali turnirski dobro
radi pa je OK. Isprobao sam i višedretvenost za evaluaciju, no nije dalo
poboljšanje jer evaluacija nije dovoljno mukotrpna.

Kod višekriterijske optimizacije očekivano je NSGA-III dao puno bolje rezultate
od moje implementacije Predator-Prey algoritma jer čuva elitizam i raznolikost
rješenja. Predator-Prey često daje ovakvo grupiranje rješenja po jednom
kriteriju po kojem mu je lakše pronaći dobra rješenja. Konkretno, ovdje je našao
puno rješenja jako i jednako dobra po Constraint riziku, a različito dobra po
Single-Resouce riziku. To nije dobro jer su sva ova rješenja u repu beskorisna.
Ovo rješenje je bolje od svih ovih ostalih jer je ono jednako po Constraint
riziku, a bolje po Single Resource riziku, pa samo njega ima smisla odabrati.
NSGA-III daje raznolikiju frontu, pa korisnik ima veći izbor. Predator-Prey
ostavlja cijeli ovaj ovdje prostor prazan.
--------------------------------------------------------------------------------


------------------------------- CONCLUSION [9] ---------------------------------
Za zaključak: s trenutnom verzijom ovog mojeg optimizatora preporučam koristiti
troturniski genetski algoritama za jednokriterijsku optimizaciju, a NSGA-III za
višekriterijsku optimizaciju. Generalno, preporučam višekriterijsku optimizaciju
iz razloga što ne treba unaprijed odrediti važnost kriterija.

Ovdje su navedena proširenja koja su bila predložena na sastancima tijekom
semestra. Ovo je prilično osnovni model za opis sigurnosti, ali bez konkretnog
primjera aplikacije teško je zaključiti koje dijelove modela treba popraviti i
proširiti. Kad sam radio eksperimentiranja na manjoj aplikaciji, pretpostavio
sam koje bi rješenje trebalo biti najbolje, i već na toj maloj aplikaciji izlaz
me je znao iznenaditi i kad bi na papiru izračunao je li to ponuđeno rješenje
bolje od mojeg, stvarno bi dobio je, pa mislim da već ovaj program može pomoći u
donošenju odluke oko razmještaja.
--------------------------------------------------------------------------------
