# PROJECT STRUCTURE:
#      ProjectRoot
#       |- tex       	# LaTeX file.
#		|- pdf		 	# PDF document.
#		|- auxiliary    # All nonessential files (auxiliary files).   


# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
# Document name without extension (PDF is assumed).
DOCUMENT = Presentation
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: $(DOCUMENT).pdf all clean purge
# ------------------------------------------------------------------------------


# ------------------------------- DEFAULT TARGET -------------------------------
# Build the main document.
all: $(DOCUMENT).pdf

# Build the PDF from LaTeX file with the same name.
$(DOCUMENT).pdf: $(DOCUMENT).tex
	latexmk -pdf -pdflatex="pdflatex -halt-on-error" -use-make $(DOCUMENT).tex
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove all nonessential files, but keep PDF document.
clean:
	latexmk -c
	rm -f $(DOCUMENT).bbl $(DOCUMENT).nav $(DOCUMENT).snm

# Remove all nonessential files, including the PDF document.
purge:
	latexmk -CA
	rm -f $(DOCUMENT).bbl $(DOCUMENT).nav $(DOCUMENT).snm
# ------------------------------------------------------------------------------
