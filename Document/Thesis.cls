% ----------------------------- CLASS INFORMATION ------------------------------
% Version of LaTeX to use.
\NeedsTeXFormat{LaTeX2e}

 % Name of the class.
\newcommand{\@classname}{thesis}
\ProvidesClass{\@classname}

% Package that enables better syntax for conditional statements.
\RequirePackage{ifthen}
% ------------------------------------------------------------------------------


% ------------------------------- GENERAL STYLE --------------------------------
% Page style. When first chapter is reached, this is overridden with page style
% that contains header and footer.
\pagestyle{plain}

% Document class to build this class upon.
\LoadClass[12pt, oneside, onecolumn]{report}

% Set paper size and margins.
\RequirePackage[a4paper,
                headheight=1cm,
                left=3cm,
                right=2.5cm,
                bottom=2.5cm,
                top=2.5cm]{geometry}

% Main font.
\RequirePackage{lmodern}

% Monospace font (teletypewriter font).
\RequirePackage{courier}

% Caption style.
\RequirePackage{caption} % Enable captions.
\renewcommand{\captionlabelfont}{\bfseries}
\renewcommand{\captionfont}{\small}

% Set line spacing. Note that this is a factor that multiplies the actual
% spacing, so experiment with this value to get wanted results (a value of 1.25
% should be more or less equal to 1.5 line spacing in Microsoft Word).
\linespread{1.25}

% Justification. Enable space stretching to fill the line.
\sloppy

% Disable warnings about under filled rows since they normally occur when
% hyphenation is restricted and sloppy justification is used.
\hbadness=10000

% Set spacing before and after sections and subsections.
\RequirePackage{titlesec} % Enable setting space before and after sections.
\titlespacing*{\section}         % Set section spacing.
    {0pt}                        % Left spacing
    {2.5ex plus 1ex minus .2ex}  % Spacing before.
    {1.8ex plus .1ex}            % Spacing after.
\titlespacing*{\subsection}      % Set subsection spacing.
    {0pt}                        % Left spacing.
    {1.5ex plus .5ex minus .1ex} % Spacing before.
    {1ex plus .1ex}              % Spacing after.
%  ------------------------------------------------------------------------------


% --------------------------- LANGUAGE AND ENCODING ----------------------------
% Encoding of the document source.
\RequirePackage[utf8]{inputenc}

% Document language.
\RequirePackage[croatian]{babel}

% Better support for quotation signs (context and language aware).
\RequirePackage{csquotes}

% Minimum number of letters before and after in order for hyphenation to occur
% for Croatian language. Fist digit is number of letters before the hyphenation
% and second is after hyphenation.
\providehyphenmins{croatian}{77}

% Command for English term in Croatian text.
\newcommand{\engl}[1]{(engl.~\emph{#1})}
% ------------------------------------------------------------------------------


% ------------------------------- NUMBERING ------------------------------------
% Literature numbering and indexing.
\RequirePackage[nottoc]{tocbibind}

% Set section numbering style.
\renewcommand{\thesection}{\arabic{section}}

% Add period after section name.
\renewcommand{\@seccntformat}[1]{\csname the#1\endcsname.\quad}

% Add period after section name inside table of contents.
\let \savenumberline \numberline
\renewcommand{\numberline}[1]{\savenumberline{#1.}}

% Enable modifying numbering counters.
\RequirePackage{chngcntr}

% Number document elements from within the chapter.
\counterwithin{section}{chapter}  % Section numbering.
\counterwithin{figure}{chapter}   % Figure numbering.
\counterwithin{equation}{chapter} % Equation numbering.
\counterwithin{table}{chapter}    % Table numbering.

% Roman page numbering until first chapter begins.
\renewcommand{\thepage}{\roman{page}}
% ------------------------------------------------------------------------------


% ----------------------- MANDATORY DOCUMENT INFORMATION -----------------------
% Command for defining the thesis number. Make it mandatory.
\newcommand{\thesisnumber}[1]{\renewcommand{\@thesisnumber}{#1}}
\newcommand{\@thesisnumber}{ % Error if no thesis number has been defined.
    \ClassError{\@classname}{No thesis number defined}
        {Define thesis number using the \noexpand\thesisnumber command.}}

% Make document author mandatory.
\renewcommand{\@author}{ % Error if no document author has been defined.
    \ClassError{\@classname}{No document author defined}
        {Define author using \noexpand\author command.}}

% Make document title mandatory.
\renewcommand{\@title}{ % Error if no document title has been defined.
    \ClassError{\@classname}{No document title defined}
        {Define title using \noexpand\title command.}}

% Make English document title.
\newcommand{\englishtitle}[1]{\renewcommand{\@englishtitle}{#1}}
\newcommand{\@englishtitle}{
    \ClassError{\@classname}{No english document title defined}
        {Define english document title using \noexpand\englishtitle command.}}
% ------------------------------------------------------------------------------


% ----------------------------------- LISTS ------------------------------------
% Package that enables customization.
\RequirePackage{enumitem}

% List symbols definitions.
\setlist[itemize,1]{label=\textendash} % First level are dashes.
\setlist[itemize,2]{label=\textbullet} % Second level are bullets.

% Remove vertical spacing.
\setlist{noitemsep}
% ------------------------------------------------------------------------------


% ------------------------------- ACKNOWLEDGMENT -------------------------------
% Flag that signals if acknowledgment has been added. Assume no acknowledgment.
\newboolean{acknowledgmentadded} \setboolean{acknowledgmentadded}{false}

% Command for acknowledgment page. Page number is predefined.
\newcommand{\acknowledgment}[1]{
    \newpage
    \setcounter{page}{3}
    \vspace*{\fill}
    \hfil {\itshape #1} \hfil
    \vspace*{\fill}
    \setboolean{acknowledgmentadded}{false} % Signal acknowledgment added.
}
% ------------------------------------------------------------------------------


% ----------------------------- TABLE OF CONTENTS ------------------------------
% Command for table of contents page. If acknowledgment is not page number is
% set to 4 and it counting starts from table of contents since empty page is
% inserted instead of acknowledgment page.
\renewcommand{\tableofcontents}{
    \chapter*{\contentsname}
    \ifthenelse{\boolean{acknowledgmentadded}}{}{\setcounter{page}{4}}
    \@starttoc{toc}
}

% Set the style of Table of Contents title.
\RequirePackage{tocloft}
\renewcommand{\cftbeforetoctitleskip}{0pt}           % Space before ToC title.
\renewcommand{\cfttoctitlefont}{\Huge \bfseries \sc} % Font of ToC title.
\renewcommand{\cftaftertoctitle}{\vspace*{-15pt}}    % Space after ToC title.
% ------------------------------------------------------------------------------


% ----------------------- CHAPTER NUMBERING AND APPENDIX -----------------------
% Create boolean flag that will be set to true when appendix has been reached.
% This is used to signal that chapter and page numbering needs to be changed.
\newboolean{atappendix} \setboolean{atappendix}{false}

% Create boolean flag that will be set to true when first chapter has been
% reached. This is used to signal that normal (Arabic) page numbering should
% start since previous pages are table of contents, acknowledgments and title.
\newboolean{atfirstchapter} \setboolean{atfirstchapter}{false}

% Command that starts appendix. Starts counting chapters with letters.
\renewcommand{\appendix}{
    \setboolean{atappendix}{true}
    \setcounter{chapter}{0}
    \renewcommand{\thechapter}{\Alph{chapter}}
}

% Space before and after chapter title.
\newcommand{\@spacebeforechaptertitle}{30pt} % Space before chapter title.
\newcommand{\@spaceafterchaptertitle}{20pt}  % Space after chapter title.

% Renew chapter command.
\renewcommand{\@makechapterhead}[1]{
    \ifthenelse{\boolean{atappendix}}{ % If appendix has been reached.
        \begin{flushleft}
            \Huge\bfseries Dodatak \thechapter \\ #1 \par
        \end{flushleft}
        \vspace*{\@spaceafterchaptertitle}
    }
    {   % Appendix has not yet been reached.
        % When this is called for the first chapter, start the standard Arabic
        % page counter. But only for the first chapter.
        \ifthenelse{\boolean{atfirstchapter}}{} % First chapter reached.
        {
            \setcounter{page}{1}
            \renewcommand{\thepage}{\arabic{page}}
            \setboolean{atfirstchapter}{true}
        }
        \vspace*{\@spacebeforechaptertitle} % Space before chapter title.
        \begin{flushleft}
            \Huge \bfseries \thechapter.~#1 \par
        \end{flushleft}
        \vspace*{\@spaceafterchaptertitle}  % Space after chapter title.
    }
}

% Renew starred chapter command. Both normal and starred version should be very
% similar. Starred version is used for table of contents and literature.
\renewcommand{\@makeschapterhead}[1]{
    \vspace*{\@spacebeforechaptertitle}
    \begin{flushleft}
        \Huge \scshape #1 \par
    \end{flushleft}
    \vspace*{\@spaceafterchaptertitle}
}
% ------------------------------------------------------------------------------


% --------------------------------- TITLE PAGE ---------------------------------
% Command for getting the current month in Croatian.
\newcommand*{\monthcroatian}{
    \ifcase\month\or
        sije\v{c}anj\or velja\v{c}a\or o\v{z}ujak\or travanj\or svibanj\or
        lipanj\or srpanj\or kolovoz\or rujan\or listopad\or studeni\or
        prosinac\fi
}

% Define title page.
\renewcommand{\maketitle}{
    \begin{titlepage}
        \let\footnotesize\small
        \let\footnoterule\relax
        \begin{center}
            \large\sffamily SVEU\v{C}ILI\v{S}TE U ZAGREBU \\
            \large\bfseries\sffamily FAKULTET ELEKTROTEHNIKE I RA\v{C}UNARSTVA
        \end{center}
        \vfill
        \begin{center}
            {\large \sffamily DIPLOMSKI RAD br.~\@thesisnumber \par}
            \vspace*{1em}
            {\huge \bfseries \sffamily \@title \par}
            \vspace*{1.75em}
            \begin{tabular}[t]{c}
                {\large \sffamily \@author}
            \end{tabular}\par
        \end{center}\par
        \vfill
        \begin{center}
            \sffamily\large Zagreb, srpanj\space \number\year.
        \end{center}
    \end{titlepage}
    \setcounter{footnote}{0}
}
% ------------------------------------------------------------------------------


% --------------------------------- ABSTRACT -----------------------------------
% English abstract environment.
\renewenvironment{abstract}
{
    \vspace*{\fill}
    \thispagestyle{empty}
    \begin{center}
        {\bf \@englishtitle}
    \end{center}
    \hspace*{\fill} {\bf Abstract} \hspace*{\fill} \par
    \vspace*{25pt}
}
{
    \vspace*{\fill}
}

% Croatian abstract environment. Assumes title is Croatian.
\newenvironment{abstractcroatian}
{
    \newpage
    \vspace*{\fill}
    \thispagestyle{empty}
    \begin{center}
        {\bf \@title}
    \end{center}
    \hspace*{\fill} {\bf Sa\v{z}etak} \hspace*{\fill} \par
    \vspace*{25pt}
}
{
    \vspace*{\fill}
}
% ------------------------------------------------------------------------------


% --------------------------------- KEYWORDS -----------------------------------
% English keywords command.
\newcommand{\keywords}[1]{
    \vspace{15pt}
    \noindent \textbf{Keywords:} #1
}

% Croatian keywords command.
\newcommand{\keywordscroatian}[1]{
    \vspace{15pt}
    \noindent \textbf{Klju\v{c}ne rije\v{c}i:} #1
}
% ------------------------------------------------------------------------------


% ---------------------------- HEADER AND FOOTER -------------------------------
% Packages required for enabling and styling header and footer.
\RequirePackage{xcolor}   % Enable defining and referencing colors.
\RequirePackage{fancyhdr} % Enable header and footer.

% Header and footer text color.
\definecolor{headerfootercolor}{HTML}{A4A4A4}

% Header style (left, center and right).
\lhead{\textcolor{headerfootercolor}{\hspace{1pt} \@author}}
\chead{}
\rhead{\textcolor{headerfootercolor}{\@title} \hspace{1pt}}
\renewcommand{\headrulewidth}{2px}
\renewcommand{\headrule} {
    \hbox to \headwidth{
        \color{lightgray} \leaders \hrule height \headrulewidth \hfill
    }
}

% Footer style (left, center and right).
\lfoot{}
\cfoot{}
\rfoot{\textcolor{headerfootercolor}{\small{\thepage} \hspace{15pt}}}
\renewcommand{\footrulewidth}{1px}
\renewcommand{\footrule}{
    \hbox to \headwidth{
        \color{lightgray} \leaders \hrule height \footrulewidth \hfill
    }
}

% Make start of the chapter not override current page style. This enables manual
% setting of the page style.
\RequirePackage{etoolbox} % Enables patching of commands.
\patchcmd{\chapter}{\thispagestyle{plain}}{}{}{}

% Define command for enabling header and footer.
\newcommand{\enableheaderandfooter}{
    \newpage
    \pagestyle{fancy}
}

% Define command for disabling header and footer.
\newcommand{\disableheaderandfooter}{
    \newpage
    \pagestyle{plain}
}
% ------------------------------------------------------------------------------


% -------------------------------- BIBLIOGRAPHY --------------------------------
% Package to use for managing bibliography. 
\RequirePackage[citestyle=numeric,
                bibstyle=numeric,
                backend=biber,
                sorting=none]{biblatex}

% Remove parenthesis around year for articles.
\renewbibmacro*{issue+date}{
    \setunit{\addcomma\space}
    \iffieldundef{issue}
        {
            \usebibmacro{date}
        }
        {
            \printfield{issue}
            \setunit*{\addspace}
            \usebibmacro{date}
        }
    \newunit
}

% Separator between bibliography sections (for example, author and title).
\renewcommand{\newunitpunct}{\addcomma\space}

% Display first and last name of the author in small capital letters.
\renewcommand{\mkbibnamegiven}[1]{\textsc{#1}}
\renewcommand{\mkbibnamefamily}[1]{\textsc{#1}}

% Title of the work displayed in bold font.
\DeclareFieldFormat*{title}{\textbf{#1}}
% ------------------------------------------------------------------------------


% ------------------------------------ IMAGES ----------------------------------
% Enable image importing.
\RequirePackage{graphicx}

% Enable text-like positioning behavior for figures.
\RequirePackage{float}

% Command for inserting standard figure. This figure can be moved around the
% document by LaTeX to better fill the document space. LaTeX usually places
% figures at the beginning or the end of the page.
\newcommand{\standardfigure}[4]
{
    \begin{figure}
        \centering
        \includegraphics[#2]{#1}
        \captionsetup{justification=centering}
        \caption{#3}
        \label{#4}
    \end{figure}
}

% Command for inserting floating figure. Floating means that the figure is
% inserted at the exact position where it is in the document source - LaTeX will
% not move it around to better fill the document.
\newcommand{\floatingfigure}[4]
{
    \begin{figure}[H]
        \centering
        \includegraphics[#2]{#1}
        \captionsetup{justification=centering}
        \caption{#3}\label{#4}
    \end{figure}
}

% Enable subfigures.
\RequirePackage{subfig}

% Command for inserting double standard figure. This figure can be moved around
% the document by LaTeX to better fill the document space. LaTeX usually places
% figures at the beginning or the end of the page.
\newcommand{\doublestandardfigure}[6]
{
    \begin{figure}
        \centering
        \subfloat[#2]{\includegraphics[width=0.4\textwidth]{#1}}
        \hspace{0.1\textwidth}
        \subfloat[#4]{\includegraphics[width=0.4\textwidth]{#3}}
        \caption{#5}
        \label{#6}
    \end{figure}
}

% Command for inserting double floating figure. Floating means that the figure
% is inserted at the exact position where it is in the document source - LaTeX
% will not move it around to better fill the document.
\newcommand{\doublefloatingfigure}[6]
{
    \begin{figure}[H]
        \centering
        \subfloat[#2]{\includegraphics[width=0.4\textwidth]{#1}}
        \hspace{0.1\textwidth}
        \subfloat[#4]{\includegraphics[width=0.4\textwidth]{#3}}
        \caption{#5}
        \label{#6}
    \end{figure}
}
% ------------------------------------------------------------------------------


% ------------------------ CODE AND PSEUDOCODE LISTINGS ------------------------
% Package that enables code listings.
\RequirePackage{listings}

% Translate listings caption to Croatian. 
\renewcommand{\lstlistingname}{Isječak}
\renewcommand{\lstlistlistingname}{Popis isječaka programskog koda}

% Enable additional text symbols. Needed for center tilde. 
\RequirePackage{textcomp}

% Define general settings for code listing environment.
\lstset{
    basicstyle={\ttfamily \small},
    columns=fullflexible,
    showstringspaces=false,
    captionpos=below,
    commentstyle=\color{gray}\upshape,
    literate={~}{\texttildelow}{1} % Center the tilde.
}

% Command for command line listing.
\newcommand{\commandline}[1]{\\{\ttfamily \small \hspace*{50pt} #1}\\}

% Command for inline code.
\newcommand{\code}[1]{{\ttfamily #1}}

% Enable pseudocode listing.
\RequirePackage[ruled,algochapter]{algorithm2e}

% Translate pseudocode listing caption to Croatian. 
\renewcommand{\algorithmcfname}{Pseudokod}

% Environment for listing pseudocode in Croatian.
\newenvironment{pseudocode}
{
    \begin{algorithm}
    \SetKwIF{If}{ElseIf}{Else}{ako}{onda}{inače ako}{inače}{kraj}
    \SetKwFor{While}{dok}{radi}{kraj}
    \SetKwFor{ForEach}{za svaki}{radi}{kraj}
}
{
    \end{algorithm}
}

% Define XML language syntax highlighting.
\lstdefinelanguage{XML}
{
    morestring=[b]{"},
    morecomment=[s]{<?}{?>},
    morecomment=[s]{<--}{-->},
    keywordstyle=\textbf,
    morekeywords={application, component, input, output,resources, fragment,
                  importance, result, deployment, server, trust, security,
                  constraint, resource, algorithm, type, population_size,
                  generation_max, log_frequency, mutation_rate, evaluators,
                  evaluator, coefficient, plot_frequency, pause_after_plot,
                  plot_range}
}
% ------------------------------------------------------------------------------
